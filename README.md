# openvas-autotask

**!!! DEPRECATED !!!**  
**DOES NOT WORK ANYMORE**  
These scripts use the old `omp` command which was part of `openvas-cli`. Both are deprecated not available anymore. 
Unless you have a very old and outdated installation of `openvas` these scripts won't work anymore. 
```
Copyright (c) 2018-2020 thomas.zink _at_ uni-konstanz _dot_ de
Usage of the works is permitted provided that this instrument is retained with the works, so that     any entity that uses the works is notified of this instrument.
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
```

Automatically create tasks using a config file and the Openvas Management Protocol (omp).

This script automatically creates targets, alerts, schedules, and tasks using a config file as input. The config file has to have the following format:

```
COMMENT="" # used for <comment/>, best use a common identifier
NAME=""    # the service name, used for <name/>
EMAIL=""   # email address used for alerts
TARGETS="" # csv of target hosts and nets
CONFIG=""  # the scan config, $FAST | $FULT | $DEEP | $DULT
DATE=""    # for schedule, either in US, EU, or ISO format
TIME=""    # for schedule, in "HH:MM"
PERIOD=""  # scan period, in "F U", where F is a number, U is day|week|month|year
```

The OpenVAS CLI requires authentication of the admin user to write and read most of the configuration. Reading the password from a file is not supported, so the username and password must be passed to the command via command line parameters. This script reads the username and password from a config file. The default file is `./etc/openvas-task.conf` and must have the following content:

```
# file: ./etc/openvas-task.conf
USER="admin"                    # username of the omp admin user, default is 'admin'
PASS="<admin user password>"    # password of omp admin user. beware special chars
```

## Content

* `bin/openvas-task`

  main application, to create and delete tasks
* `bin/sanitize-config`

  parses and sanitizes a config file and outputs the sanitized input to `stdout`
* `bin/openvas-inotify`

  inotfy watchscript to automatically sanitize and add config files on CLOSE event
* `lib/openvas-config.shlib`

  provides functions to read and evaluate config files and set options accoridngly
* `lib/readconfig.shlib`

  general library to read sh config files securely
* `lib/sanitize.shlib`

  general library that provides various input sanitization functions
* `etc/openvas-task.conf`

  user conf for credentials
* `conf/example.conf`

  example configuration file
* `conf/malicious.conf`

  example malicious conf file, that's missing options and tries to inject code

## Usage

usage: ./openvas-autotask [-c | -d <file>] options -c <file>: create openvas task and dependencies -d <file>: delete openvas task and dependencies <file>: path of the task config file -u <user>: OMP username (default: admin) -w <pass>: OMP password

An example config file:

```
# file "example.conf"
COMMENT="2013223598752319"
NAME="scanme"
EMAIL="admin@example.com"
TARGETS="scanme.org, scanme.nmap.org"
CONFIG="$DEEP"
DATE="2018-12-10" # | "10.12.2018" | "12/10/2018"
TIME="22:30"
PERIOD="1 week"	# every week
```

Example usage:

Create task, use `etc/openvas-task.conf` for username and password

```
# ./openvas-task -c example.conf
```

Create task, explicit pass of the admin password:

```
# ./openvas-task -w IamgRoot -c example.conf
```

Delete targets, schedule, alert, and task, explicit pass of admin user and pass:

```
./openvas-task -u groot -w IamgRoot -d example.conf
```
